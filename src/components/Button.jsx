import { React, useState } from 'react'
import { Button, Box, Modal, TextField, Grid } from '@mui/material'
// import List from './List'

const AddButton = () => {
    const [trigger, setTrigger] = useState(false);
    const [info, setInfo] = useState({
        name: '',
        address: '',
        hobby: ','
    });
    const [data] = useState([]);

    // let data = []

    // const handleClick = () => {
    //     data.map(() => {})
    // }

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    return (
        <>
            <div className='navbar'>
                <Box display='flex' justifyContent='space-between'>
                    <span>My App</span>
                    <Button onClick={() => { setTrigger(true) }} sx={{ borderRadius: 28 }} variant="contained" >Add User</Button>
                </Box>
            </div>

            <div>

                <Modal
                    open={trigger}
                    onClose={() => setTrigger(false)}
                    aria-labelledby="parent-modal-title"
                    aria-describedby="parent-modal-description">
                    <Box sx={{ ...style, width: 400 }}>
                        <label>Name</label>
                        <TextField sx={{ borderRadius: 28, paddingBottom: '10px', paddingTop: '10px' }} fullWidth variant='outlined' onChange={(e) => setInfo({ ...info, name: e.target.value })} />
                        <label>Address</label>
                        <TextField sx={{ borderRadius: 28, paddingBottom: '10px', paddingTop: '10px' }} fullWidth variant='outlined' onChange={(e) => setInfo({ ...info, address: e.target.value })} />
                        <label>Hobby</label>
                        <TextField sx={{ borderRadius: 28, paddingBottom: '10px', paddingTop: '10px' }} fullWidth variant='outlined' onChange={(e) => setInfo({ ...info, hobby: e.target.value })} />
                        <div className='btn-save'>
                            <Button variant='contained' onClick={() => { data.push(info); setTrigger(false) }}>Save</Button>
                        </div>

                    </Box>
                </Modal>

            </div >

            {(data.length === 0) ?
                <div className='text'>
                    <h1>
                        O User
                    </h1>
                </div>
                :
                <div>
                    {data.map((e) => {
                        return (
                            <>
                                {/* <Grid container border='solid'>
                                    <Grid display='flex' justifyContent='left'>
                                        <h6 key={e.name}>Nama {e.name}</h6>
                                        <h6 key={e.name}>{e.address}</h6>
                                    </Grid>
                                    <Grid display='flex' justifyContent='right'>
                                        <h4 key={e.name}>{e.hobby}</h4>
                                    </Grid>
                                </Grid> */}

                                {/* <Grid container border='solid'>
                                    <Grid item xs={6}>
                                        <h6 key={e.name}>Nama {e.name}</h6>
                                        <h6 key={e.name}>{e.address}</h6>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <h4 key={e.name}>{e.hobby}</h4>
                                    </Grid>
                                </Grid> */}
                                <div class="container">
                                    <h4 className='item1' key={e.name}>Nama {e.name}</h4>
                                    <h4 className='item2' key={e.name}>{e.address}</h4>
                                    <h2 className='item3' key={e.name}>{e.hobby}</h2>
                                </div>

                                {/* <List /> */}
                            </>
                        );
                    })}
                </div>
            }
        </>
    )
}

export default AddButton